import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';

import { LoginPage } from '../login/login';
import { OrdersPage } from '../orders/orders';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public project_title:string;
  public project_sub_title:string;

  constructor(private navCtrl: NavController, private auth:AuthProvider) { }

  ionViewDidLoad () {
    // set variables
    this.project_title = this.auth.vars.project_title;
    this.project_sub_title = this.auth.vars.project_sub_title;

    // check if user already has a valid token
    if (this.auth.authenticate ()) {
      // if user already login the redirect to order page
      this.navCtrl.push (OrdersPage);
    } else {
      // set if user not logged in then redirect to login
      this.navCtrl.push (LoginPage);
    }
  }
}
