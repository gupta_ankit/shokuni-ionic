import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { OrdersProvider } from '../../providers/orders/orders';

import { OrderDetailPage } from '../order-detail/order-detail';

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

  protected project_title;
  protected waiting_orders:Array<Object>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private auth:AuthProvider, 
    private order: OrdersProvider
  ) { }

  ionViewDidLoad() {
    this.project_title = this.auth.vars.project_title;
    this.getWaitingOrOngoingOrders();
  }

  ionViewCanEnter() {
    return this.auth.authenticate ();
  }

  // Method to get waiting orders
  protected getWaitingOrOngoingOrders () {
    // show loader
    this.auth.vars.displayLoader();

    let values = {'status':'on-chair,waiting'};

    this.order.getOrders ({}).subscribe (res => {
      // get waiting orders
      this.waiting_orders = res.data.data;
      // hide loader
      this.auth.vars.dismissLoader();
      this.auth.vars.presentToast ({message: res.message});
    }, error => {
      // display error toaster
      this.auth.vars.presentToast ({message: (JSON.stringify(error.errors)).replace(/[^a-zA-Z\d\s:,]/g, " ").replace(/,/g, "<br/>")});
      // hide loader
      this.auth.vars.dismissLoader();
    });
  }

  /**
   * Method to calculate service charge
   * from Order response
   * 
   * @param Object service
   * @param Object/null Extra Service
   * 
   * @return number
   */
  calculateServiceCharge (service:Object, extra_service:Object = null) {
    let service_charge = service['price'];
    
    // add extra-service charge
    if (extra_service) {
      for (let index in extra_service) {
        // add extra-service charge in charges
        service_charge += extra_service[index]['price'];
      }
    }

    return parseFloat (service_charge).toFixed (2);
  }

  /**
   * Method to get Service information
   * 
   * @param Object service
   * @param Object/null Extra Service
   * 
   * @return string
   */
  getServiceName (service:Object, extra_service:Object = null) :string {
    return service['name'] + ((extra_service) ? ' Extra' : '');
  }

  /**
   * Method to get complete detail of order
   * 
   * @param Order order
   * @param string status
   * 
   * @return void
   */
  getOrderDetail (order_uuid:string):void {
    this.navCtrl.push (OrderDetailPage, {order_uuid:order_uuid});
  }
}
