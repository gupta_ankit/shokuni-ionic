import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams  } from 'ionic-angular';

import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { AuthProvider } from '../../providers/auth/auth';

import { OrdersPage } from '../orders/orders';
import { RegisterPage } from '../register/register';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm:FormGroup;

  protected project_title;
  protected page_title:string = "login";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private auth:AuthProvider
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.maxLength (20)])
    });
  }

  ionViewDidLoad() {
    this.project_title = this.auth.vars.project_title;
  }

  loginSubmit (form:NgForm) {
    // show loader
    this.auth.vars.displayLoader();

    // collect login form values
    let values = {
      username: (form.controls.username.value),
      password: (form.controls.password.value)
    };

    // request server for user login
    this.auth.loginUser(values).subscribe(login => {
      // hide loader
      this.auth.vars.dismissLoader();

      this.auth.vars.presentToast ({message: login.message});

      // save login-information in memory
      localStorage.setItem('login_info', JSON.stringify (login));

      // redirect to barber's order
      this.navCtrl.push (OrdersPage);
    }, error => { this.handleError(error) })
  }

  private handleError (error) {
    // hide loader
    this.auth.vars.dismissLoader();

    // check if server error has errors key
    if (error.errors) {      
      // display error toaster
      this.auth.vars.presentToast ({message: (JSON.stringify(error.errors)).replace(/[^a-zA-Z\d\s:,]/g, " ").replace(/,/g, "<br/>")});
    }
  }

  ionViewCanEnter() {
    return !this.auth.authenticate ();
  }

  /**
   * Method to render user on login page
   */
  register():void {
    this.navCtrl.push (RegisterPage);
  }
}
