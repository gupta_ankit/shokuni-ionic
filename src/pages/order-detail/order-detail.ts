import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { OrdersProvider } from '../../providers/orders/orders';
import { AlertController } from 'ionic-angular';

import { OrdersPage } from '../orders/orders';
import { OrderPaymentPage } from '../order-payment/order-payment';

/**
 * Generated class for the OrderDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {

  protected order_uuid:string;
  protected order_detail:Object;
  protected order_remark:string = null;
  protected detail: string = 'order-detail';

  constructor(
    public navCtrl: NavController, 
    private navParams: NavParams,
    private auth:AuthProvider,
    private order:OrdersProvider,
    private alertCtrl: AlertController
  ) {  }

  ionViewDidLoad() {
    this.order_uuid = this.navParams.get ('order_uuid');

    this.getOrderDetail ();
  }

  ionViewCanEnter() {
    return this.auth.authenticate ();
  }

  /**
   * Method to get complete detail of an order
   */
  protected getOrderDetail () {
    // show loader
    this.auth.vars.displayLoader();
    this.order.getOrderDetail (this.order_uuid).subscribe (res => {
      // get order detail
      this.order_detail = res.data;
      // hide loader
      this.auth.vars.dismissLoader();
      this.auth.vars.presentToast ({message: res.message});
    }, error => {
      // display error toaster
      this.auth.vars.presentToast ({message: (JSON.stringify(error.errors)).replace(/[^a-zA-Z\d\s:,]/g, " ").replace(/,/g, "<br/>")});
      // hide loader
      this.auth.vars.dismissLoader();
    });
  }

  /**
   * Method to display color or action button
   * 
   * @param string status
   * 
   * @return string color
   */
  statusColor(status:string):string {
    let color = 'light';

    switch (status) {
      case 'waiting':
        color = 'primary';
        break;
      case 'left':
      case 'removed':
        color = 'dark';
        break;
      case 'completed':
        color = 'secondary';
        break;
      default:
        color = 'danger';
        break;
    }
    return color;
  }

  /**
   * Method to display button for order
   * 
   * @param status 
   * 
   * @return array
   */
  getOrderStatusBtn (status:string):Array<any> {
    switch (status.toLowerCase()) {
      case 'waiting':
        return [{name: 'accept', color: 'primary', click_status: 'on-chair', remark_required: false}, {name: 'reject', color: 'danger', click_status: 'removed', remark_required: true}];
      case 'on-chair':
        return [{name: 'completed', color: 'primary', click_status: 'completed', remark_required: false}];
    }
    return [];
  }

  /**
   * Method to ask for comment
   */
  setComment (status:string = null) {
      let alert = this.alertCtrl.create({
        title: 'Remark',
        inputs: [
          { name: 'remark', placeholder: 'Remark', value: this.order_remark }
        ],
        buttons: [
          { text: 'Cancel', role: 'cancel', handler: data => { this.order_remark = null; } },
          {
            text: 'Remark',
            handler: data => {
              // set remark
              this.order_remark = data.remark;
              // check if status given
              if (status) {
                // submit the order
                this.updateBarberOrder (status);
              }
            }
          }
        ]
      });
      alert.present();
  }

  /**
   * Method to update order-status by barber
   * 
   * @param string status
   * @param boolean remark_required
   */
  updateBarberOrder (status:string, remark_required:boolean = false) {
    // check if comment-required
    if (remark_required && !this.order_remark) {
      // display alert box to set comment
      return this.setComment (status);
    }

    // show loader
    this.auth.vars.displayLoader();

    // set values to send on server
    let values = {status:status, remark:this.order_remark};

    // call server to update order status
    this.order.updateOrder(this.order_uuid, values).subscribe(res => {
      this.auth.vars.presentToast ({message: res.message});
      
      // hide loader
      this.auth.vars.dismissLoader();

      // redirect to orders page on several status
      if (['left', 'removed'].indexOf(status) > -1) {
        // redirect to orders page
        return this.navCtrl.push(OrdersPage);
      }

      // redirect user to payment page on completed
      if (['completed'].indexOf(status) > -1) {
        return this.navCtrl.push (OrderPaymentPage, {order_uuid:this.order_uuid});
      }
      
      // update order detail
      this.getOrderDetail ();
    }, error => {
      // display error toaster
      this.auth.vars.presentToast ({message: (JSON.stringify(error.errors)).replace(/[^a-zA-Z\d\s:,]/g, " ").replace(/,/g, "<br/>")});
      // hide loader
      this.auth.vars.dismissLoader();
    });
  }

  /**
   * Method to calculate total service time spends
   */
  calculateServiceTime (start_timestamp, end_timestamp) {
    start_timestamp = new Date (start_timestamp * 1000);
    end_timestamp = new Date (end_timestamp * 1000);

    // calculate time difference in seconds
    return Math.ceil ((end_timestamp.getTime() - start_timestamp.getTime())/1000);
  }
}
