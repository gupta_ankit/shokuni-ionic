import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderPaymentPage } from './order-payment';

@NgModule({
  declarations: [
    OrderPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderPaymentPage),
  ],
})
export class OrderPaymentPageModule {}
