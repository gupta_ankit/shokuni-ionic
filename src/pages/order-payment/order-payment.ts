import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { OrdersProvider } from '../../providers/orders/orders';
import { OrdersPage } from '../orders/orders';

/**
 * Generated class for the OrderPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-payment',
  templateUrl: 'order-payment.html',
})
export class OrderPaymentPage {

  // private discount_type:boolean = false;
  private isChecked:boolean = false;
  private discount:number = 0;
  private amount_paid:number = 0;
  private orderAmt:number = 0;

  public order_uuid:string;
  protected order_detail:Object;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private auth:AuthProvider, 
    private order:OrdersProvider
  ) {
  }

  ionViewDidLoad() {
    this.order_uuid = this.navParams.get ('order_uuid');
    // gather order detail
    this.getOrderDetail ();
  }

  /**
   * Method to get complete detail of an order
   */
  protected getOrderDetail () {
    this.order.getOrderDetail (this.order_uuid).subscribe (res => {
      // get order detail
      this.order_detail = res.data;
      this.amount_paid = this.orderAmt = this.orderTotal();
    }, error => {
      this.auth.vars.presentToast ({message: (JSON.stringify(error.errors)).replace(/[^a-zA-Z\d\s:,]/g, " ").replace(/,/g, "<br/>")});
    });
  }

  ionViewCanEnter() {
    return this.auth.authenticate ();
  }

  /**
   * Method to calculate total service time spends
   */
  calculateServiceTime (start_timestamp, end_timestamp) {
    start_timestamp = new Date (start_timestamp * 1000);
    end_timestamp = new Date (end_timestamp * 1000);

    // calculate time difference in seconds
    return Math.ceil ((end_timestamp.getTime() - start_timestamp.getTime())/1000);
  }

  /**
   * Method to get each service of order
   * 
   * @return Object
   */
  orderTotal(): number {
    let total_order_charge = 0;

    if (this.order_detail) {
      total_order_charge += this.order_detail['service']['price'];

      for (let index in this.order_detail['extra_service']) {
        total_order_charge += this.order_detail['extra_service'][index]['price'];
      }
    }

    return total_order_charge;
  }

  /**
   * Method to submit payment information
   */
  paymentSubmit () {
    let values = {
      order_uuid: this.order_uuid,
      payment_mode: 'cash',
      discount: this.discount,
      discount_type: ((this.isChecked) ? 'percentage' : 'amount')
    }

    this.order.makePayment(values).subscribe(res => {
      // show success message
      this.auth.vars.presentToast ({message: res.message});
      // redirect to orde list page
      this.navCtrl.push (OrdersPage);
    }, error => {
      this.auth.vars.presentToast ({message: (JSON.stringify(error.errors)).replace(/[^a-zA-Z\d\s:,]/g, " ").replace(/,/g, "<br/>")});
    })

  }

  /**
   * method to calculate payable amount
   */
  calculateAmt ():void {
    this.orderAmt = this.amount_paid - this.discount;
    
    // check if is-check is true
    if (this.isChecked) {
      // apply discount as percentage
      this.orderAmt = (this.amount_paid * (1 - ((this.discount)/100)));
    }
  }

  doCheck ($event) {
    this.isChecked = !this.isChecked;
    this.calculateAmt();
  }
}
