import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { AuthProvider } from '../../providers/auth/auth';
import { UsersProvider } from '../../providers/users/users';

import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  registerForm:FormGroup;

  protected project_title;
  protected page_title:string = "Register";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private auth:AuthProvider, 
    private user:UsersProvider
  ) {
    this.registerForm = new FormGroup({
      username: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.maxLength (20)]),
      first_name: new FormControl (null, [Validators.maxLength (100)]),
      last_name: new FormControl (null, [Validators.maxLength (100)]),
      phone: new FormControl (null, [Validators.required, Validators.maxLength (10)]),
    });
  }

  ionViewDidLoad() {
    this.project_title = this.auth.vars.project_title;
  }

  ionViewCanEnter() {
    return !this.auth.authenticate ();
  }

  /**
   * Redirect user to login page
   */
  login ():void {
    this.navCtrl.push (LoginPage);
  }

  /**
   * Method to register user
   */
  registerSubmit (form: NgForm) {
    // request server for user login
    this.user.registerUser (this.registerForm.value).subscribe(register => {
      this.auth.vars.presentToast ({message: register.message});
      // redirect to barber's order
      this.login();
    }, error => { this.handleError(error) })
  }

  private handleError (error) {
    // check if server error has errors key
    if (error.errors) {
      // display error toaster
      this.auth.vars.presentToast ({message: (JSON.stringify(error.errors)).replace(/[^a-zA-Z\d\s:,]/g, " ").replace(/,/g, "<br/>")});
    }
  }
}
