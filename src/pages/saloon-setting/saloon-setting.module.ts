import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaloonSettingPage } from './saloon-setting';

@NgModule({
  declarations: [
    SaloonSettingPage,
  ],
  imports: [
    IonicPageModule.forChild(SaloonSettingPage),
  ],
})
export class SaloonSettingPageModule {}
