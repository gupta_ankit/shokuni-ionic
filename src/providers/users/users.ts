import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GlobalProvider } from '../global/global';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersProvider {

  constructor(public http: HttpClient, private vars:GlobalProvider) {  }

  registerUser (values:Object): Observable<any> {
    let url = this.vars.backend_url + '/users/register';

    // add client-role
    values['role'] = this.vars.login_role;

    return this.http.post(url, values)
                     .map(this.extractData)
                     .catch(this.handleErrorObservable);
  }

  private extractData(res) {
    return res || {};
  }

  private handleErrorObservable (error: HttpErrorResponse | any) {
    // return error.message || error;
    return Observable.throw(error.error || error.message || error);
  }

}
