import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GlobalProvider } from '../global/global';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the SaloonProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SaloonProvider {

  constructor(private http: HttpClient, public vars:GlobalProvider) { }

  addDelay(delay:number) : Observable<any> {
    let url = this.vars.backend_url + '/saloon/barber/delay';

    // get token information
    let token = JSON.parse (localStorage.getItem ('login_info'));

    // set header to send request
    let headers = new HttpHeaders({
      'Content-Type': 'text/json',
      'Authorization': (token.token_type + ' ' + token.access_token)
    });

    // send request to add delay
    return this.http.put (url, JSON.stringify({delay:delay}), {headers})
              .map(this.extractData)
              .catch(this.handleErrorObservable);
  }

  private extractData(res) {
    return res || {};
  }

  private handleErrorObservable (error: HttpErrorResponse | any) {
    // return error.message || error;
    return Observable.throw(error.error || error.message || error);
  }
}
