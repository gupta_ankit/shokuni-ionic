import { Injectable } from '@angular/core';

import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import { ToastController, LoadingController } from 'ionic-angular';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {

  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController
  ) {}

  public project_title:string = "barber dekho";
  public project_sub_title:string = "shop for everyone";

  public login_client_id:number = 2;
  public login_role:string = "barber";
  public login_client_secret:string = "tc6NkJHVEnh9WUQp3mtyNJUsbe26scWEJoZV1bsU";
  public login_grant_type:string = "password";

  public backend_url:string = 'http://localhost:8000';

  protected loading:any;

  public extractData(res) {
    return res || {};
  }

  public handleErrorObservable (error: HttpErrorResponse | any) {
    // return error.message || error;
    return Observable.throw(error.error || error.message || error);
  }

  /**
   * Display toast message
   * 
   * @param message 
   * @param duration 
   * @param position 
   * 
   * @param format: {message:string, cssClass:string, duration:number, position:string}
   * 
   * @return void
   */
  presentToast(values:Object):void {
    let toast = this.toastCtrl.create({
      message: values['message'] || null,
      duration: values['duration'] || 5000,
      position: values['position'] || 'bottom',
      showCloseButton: true,
      cssClass: values['cssClass'] || null
    });
  
    toast.onDidDismiss(() => {});
  
    toast.present();
  }

  protected designLoader () {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  }

  displayLoader () {
    this.designLoader ();
    this.loading.present();
  }

  dismissLoader () {
    this.loading.dismiss();
  }

}
