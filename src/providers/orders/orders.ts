import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GlobalProvider } from '../global/global';

import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/toPromise';

/*
  Generated class for the OrdersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrdersProvider {

  constructor(public http: HttpClient, public vars:GlobalProvider) { }

  getOrders (param:Object): Observable<any> {
    // generate URL
    let url = new URL (this.vars.backend_url + '/saloon/orders');

    // append request parameters in URL if exists
    for (let p in param) {
      url.searchParams.append(p, param[p]);
    }

    // get token information
    let token = JSON.parse (localStorage.getItem ('login_info'));

    // set header to send request
    let headers = new HttpHeaders({
      'Authorization': (token.token_type + ' ' + token.access_token)
    });

    // send request to add delay
    return this.http.get (url.href, {headers})
              .map(this.vars.extractData)
              .catch(this.vars.handleErrorObservable);
  }

  /**
   * Method to get detail of order
   * 
   * @param string order-uuid
   * 
   * @return Observable response
   */
  getOrderDetail (order_uuid:string): Observable<any> {
    // generate URL
    let url = new URL (this.vars.backend_url + '/saloon/orders/' + order_uuid);

    // get token information
    let token = JSON.parse (localStorage.getItem ('login_info'));

    // set header to send request
    let headers = new HttpHeaders({
      'Authorization': (token.token_type + ' ' + token.access_token)
    });

    // send request to add delay
    return this.http.get (url.href, {headers})
              .map(this.vars.extractData)
              .catch(this.vars.handleErrorObservable);
  }

  /**
   * Method to update order-detail information
   * 
   * @param String order_uuid
   * @param Object value
   */
  updateOrder (order_uuid:string, value:Object): Observable<any> {
    // generate URL
    let url = new URL (this.vars.backend_url + '/saloon/orders/' + order_uuid);

    // get token information
    let token = JSON.parse (localStorage.getItem ('login_info'));

    // set header to send request
    let headers = new HttpHeaders({
      'Authorization': (token.token_type + ' ' + token.access_token),
      'Content-Type' : 'application/json'
    });

    // send request to add delay
    return this.http.put (url.href, value , {headers})
              .map(this.vars.extractData)
              .catch(this.vars.handleErrorObservable);
  }

  makePayment (values:Object) {
    // generate URL
    let url = this.vars.backend_url + '/saloon/orders/payment';

    // get token information
    let token = JSON.parse (localStorage.getItem ('login_info'));

    // set header to send request
    let headers = new HttpHeaders({
      'Authorization': (token.token_type + ' ' + token.access_token)
    });

    // send request to add delay
    return this.http.post (url, values, {headers})
              .map(this.vars.extractData)
              .catch(this.vars.handleErrorObservable);
  }
}
