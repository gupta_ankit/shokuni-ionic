import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GlobalProvider } from '../global/global';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  protected isLoggedIn:boolean = false;

  constructor(public http: HttpClient, public vars:GlobalProvider) {
  }

  loginUser (values:Object): Observable<any> {
    let url = this.vars.backend_url + '/users/login';

    // add client-login required variables
    values['client_secret'] = this.vars.login_client_secret;
    values['client_id'] = this.vars.login_client_id;
    values['role'] = this.vars.login_role;
    values['grant_type'] = this.vars.login_grant_type;

    return this.http.post(url, values)
                     .map(this.extractData)
                     .catch(this.handleErrorObservable);
  }

  private extractData(res) {
    return res || {};
  }

  private handleErrorObservable (error: HttpErrorResponse | any) {
    // return error.message || error;
    return Observable.throw(error.error || error.message || error);
  }

  /**
   * Set user al logged in
   */
  private login ():void {
    this.isLoggedIn = false;
    // check localstorage has token
    if (localStorage.hasOwnProperty ('login_info') && JSON.parse (localStorage.getItem ('login_info')).access_token) {
      this.isLoggedIn = true;
    }
  }

  /**
   * check if user ogged in or not
   */
  authenticate ():boolean {
    // check if user already logged-in
    this.login ();
    return this.isLoggedIn;
  }

  protected logout () {
    localStorage.removeItem('login_info');
    this.isLoggedIn = false;
  }

  /**
   * Method to logout User
   */
  logoutUser (): Observable<any> {
    // logout user token
    let url = this.vars.backend_url + '/users/logout';

    if (localStorage.hasOwnProperty ('login_info') && JSON.parse (localStorage.getItem ('login_info')).access_token) {
      // get token information
      let token = JSON.parse (localStorage.getItem ('login_info'));

      // set header to send request
      let headers = new HttpHeaders({
        'Authorization': (token.token_type + ' ' + token.access_token)
      });

      return this.http.delete (url, {headers})
        .map(this.logout)
        .catch(this.handleErrorObservable);
    }
  }
}
