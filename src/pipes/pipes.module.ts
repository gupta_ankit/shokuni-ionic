import { NgModule } from '@angular/core';
import { OrderUuidPipe } from './order-uuid/order-uuid';
import { TimeAgoPipe } from './time-ago/time-ago';
@NgModule({
	declarations: [OrderUuidPipe,
    TimeAgoPipe,
    TimeAgoPipe],
	imports: [],
	exports: [OrderUuidPipe,
    TimeAgoPipe,
    TimeAgoPipe]
})
export class PipesModule {}
