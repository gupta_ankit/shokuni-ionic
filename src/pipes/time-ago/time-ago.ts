import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TimeAgoPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'timeAgo',
})
export class TimeAgoPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */

   readonly MINIUT_TO_SECOND = 60;
   readonly HOUR_IN_DAY = 24;

  transform(delta: number) {
    let day = Math.floor (delta / (this.MINIUT_TO_SECOND * this.MINIUT_TO_SECOND * this.HOUR_IN_DAY));
    let hour = Math.floor ((delta % (this.MINIUT_TO_SECOND * this.MINIUT_TO_SECOND * this.HOUR_IN_DAY))/(this.MINIUT_TO_SECOND * this.MINIUT_TO_SECOND));
    let miniutes = Math.floor ((delta % (this.MINIUT_TO_SECOND * this.MINIUT_TO_SECOND))/ this.MINIUT_TO_SECOND);
    let seconds = Math.floor ((delta % (this.MINIUT_TO_SECOND * this.MINIUT_TO_SECOND)) % this.MINIUT_TO_SECOND);
    return (day ? (day + 'D') : '') + ' ' + (hour ? (hour + 'H') : '') + ' ' + (miniutes ? (miniutes + 'm') : '') + ' ' + seconds + 's';
  }
}
