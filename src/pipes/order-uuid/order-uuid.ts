import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the OrderUuidPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'orderUuid',
})
export class OrderUuidPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string) {
    return (value) ? (value.match(/.{1,4}/g).join('-')).toUpperCase() : null;
  }
}
