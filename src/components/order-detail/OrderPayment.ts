export class OrderPayment {
    amount:number;
    amount_paid:number;
    created_at:number;
    discount:number;
    discount_type:string;
    payment_mode:string;
}