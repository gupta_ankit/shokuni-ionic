import { Component, Input } from '@angular/core';

@Component({
    selector: 'order-detail-services',
    templateUrl: 'order-detail-services.html'
  })
  export class OrderDetailService {

    @Input() OrderService:any;

    ngOnInit() {}

    /**
     * Method to get each service of order
     * 
     * @return Object
     */
    orderTotal(): number {
        let total_order_charge = 0;

        if (this.OrderService) {
        total_order_charge += this.OrderService['service']['price'];

        for (let index in this.OrderService['extra_service']) {
            total_order_charge += this.OrderService['extra_service'][index]['price'];
        }
        }

        return total_order_charge;
    }
}