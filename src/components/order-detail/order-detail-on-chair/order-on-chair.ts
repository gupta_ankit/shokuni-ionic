import { Component, Input } from '@angular/core';

@Component({
    selector: 'order-on-chair',
    templateUrl: 'order-on-chair.html'
  })
  export class OrderOnChair {

    @Input() ChairOrder:any;

    ngOnInit() {}

    readonly MINUIT_INTO_SECONDS = 60;
    readonly JS_TIME_STAMP = 1000;

    /**
     * Method to calculate order-services total time
     */
    calculateOrderServiceTime ():number {
      let total_order_time = 0;

      if (this.ChairOrder) {
        total_order_time += this.ChairOrder['service']['time'];

        for (let index in this.ChairOrder['extra_service']) {
          total_order_time += this.ChairOrder['extra_service'][index]['time'];
        }
      }

      return total_order_time;
    }

    /**
     * Method to calculate remaining service time
     * @in seconds
     * 
     * @return number
     */
    calculateRemainingServiceTime ():number {
      // calculate service time
      // convert service start-time into seconds
      let remaining_time = (this.calculateOrderServiceTime() * this.MINUIT_INTO_SECONDS) - Math.ceil (((new Date()).getTime () - (new Date (this.ChairOrder['start_time'] * this.JS_TIME_STAMP)).getTime ())/this.JS_TIME_STAMP);

      return (remaining_time >= 1) ? remaining_time : 0;
    }
}