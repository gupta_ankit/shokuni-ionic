import { Component, Input } from '@angular/core';
import { OrderPayment } from '../OrderPayment';

@Component({
    selector: 'order-detail-payment',
    templateUrl: 'order-detail-payment.html'
  })
  export class OrderDetailPayment {

    @Input() OrderPaymentDetail:OrderPayment;

    ngOnInit() {}

    calculateDiscount () {
      // check if discount type exists
      if (typeof this.OrderPaymentDetail['discount_type'] !== 'undefined' && this.OrderPaymentDetail['discount_type'] === null) {
        let discount = this.OrderPaymentDetail['amount'] - this.OrderPaymentDetail['discount'];

        // check if discount is in percentage
        if (this.OrderPaymentDetail['discount_type'] === 'percentage') {
          discount = this.OrderPaymentDetail['amount'] * (1- (this.OrderPaymentDetail['discount']/100));
        }

        return discount;
      }
    }
}