import { Component, Input } from '@angular/core';

@Component({
    selector: 'timeAgo',
    templateUrl: 'TimeAgo.html'
  })
  export class TimeAgo {
    @Input() ServerTimeStamp:number;
    readonly PHP_TIMESTAMP_INTO_JS_TIME:number = 1000;
    public time_diff:any;

    ngOnInit() {
        this.timerTick();
    }

    /**
     * Calculate time difference in seconds
     * 
     * @return number
     */
    calculateTimeDiffInSeconds ():number {
        return Math.ceil ( ((new Date()).getTime() - (new Date (this.ServerTimeStamp * this.PHP_TIMESTAMP_INTO_JS_TIME).getTime()))/this.PHP_TIMESTAMP_INTO_JS_TIME );
    }

    timerTick() {
        setTimeout(() => {
            this.time_diff = this.calculateTimeDiffInSeconds();
            this.timerTick();
        }, 1000);
    }
}