import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { OrdersPage } from '../pages/orders/orders';
import { RegisterPage } from '../pages/register/register';
import { OrderDetailPage } from '../pages/order-detail/order-detail';
import { SaloonSettingPage } from '../pages/saloon-setting/saloon-setting';
import { OrderPaymentPage } from '../pages/order-payment/order-payment';

// inject components
import { Timer } from '../components/countdown-timer/timer';
import { TimeAgo } from '../components/time-ago/TimeAgo';
import { OrderDetailPayment } from '../components/order-detail/order-detail-payment/order-detail-payment';
import { OrderOnChair } from '../components/order-detail/order-detail-on-chair/order-on-chair';
import { OrderDetailService } from '../components/order-detail/order-detail-services/order-detail-services';

import { GlobalProvider } from '../providers/global/global';
import { AuthProvider } from '../providers/auth/auth';
import { UsersProvider } from '../providers/users/users';
import { SaloonProvider } from '../providers/saloon/saloon';
import { OrdersProvider } from '../providers/orders/orders';

import { OrderUuidPipe } from '../pipes/order-uuid/order-uuid';
import { TimeAgoPipe } from '../pipes/time-ago/time-ago';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    OrdersPage,
    RegisterPage,
    SaloonSettingPage,
    OrderDetailPage,
    OrderPaymentPage,

    Timer,
    TimeAgo,
    OrderDetailPayment,
    OrderOnChair,
    OrderDetailService,
    
    OrderUuidPipe,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    OrdersPage,
    RegisterPage,
    OrderDetailPage,
    SaloonSettingPage,
    OrderPaymentPage,
    Timer,
    TimeAgo,
    OrderDetailPayment,
    OrderOnChair,
    OrderDetailService
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,
    AuthProvider,
    UsersProvider,
    SaloonProvider,
    OrdersProvider
  ]
})
export class AppModule {}
