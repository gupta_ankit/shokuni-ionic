import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SaloonSettingPage } from '../pages/saloon-setting/saloon-setting';
import { AuthProvider } from '../providers/auth/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  protected project_title:string;

  rootPage:any = HomePage;

  pages: Array<{title: string, component: any, icon:string}>;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    private auth:AuthProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    // set array of all components in menu
    this.pages = [
      {title: 'Orders', component: HomePage, icon: 'list'},
      {title: 'Profile', component: null, icon: 'person'},
      {title: 'Setting', component: SaloonSettingPage, icon: 'build'},
      {title: 'Logout', component: null, icon: 'log-out'}
    ]

    // get name of the project
    this.project_title = this.auth.vars.project_title;
  }

  openPage(page) {
    // check if user click on logout as no page required
    if (!page.component) {
      this.auth.vars.displayLoader();
      return this.auth.logoutUser().subscribe(res=> {
        this.nav.setRoot(HomePage);
        this.auth.vars.dismissLoader();
      }, err => {
        this.nav.setRoot(HomePage);
        this.auth.vars.dismissLoader();
      });
    } 
    
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

